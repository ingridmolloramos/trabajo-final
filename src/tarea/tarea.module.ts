import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Tarea } from 'src/usuario/entities/tarea.entity.ts';
import { TareaController } from './tarea.controller';
import { TareaService } from './tarea.service';
import { TareaRepository } from './tarea.repository';

@Module({
  imports: [TypeOrmModule.forFeature([Tarea])],
  controllers: [TareaController],
  providers: [TareaService, TareaRepository],
  exports: [TareaService],
})
export class TareaModule {}
