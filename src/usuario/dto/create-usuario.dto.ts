import { ApiProperty } from '@nestjs/swagger';
import { IsAlphanumeric, IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';

export class CreateUsuarioDto {
  @ApiProperty({
    example: 'Carlos',
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(5, {
    message: 'El nombre del usuario deberia tener al menos 5 caracteres',
  })
  nombre: string;

  @ApiProperty({
    example: 'Karen',
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(3, {
    message: 'El nickname del usuario deberia tener al menos 3 caracteres',
  })
  @IsAlphanumeric(null, { message: 'Solo se permiten numeros y letras' })
  nombreUsuario: string;

  @ApiProperty({
    example: 'karen@gmail.com',
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  @IsEmail(null, { message: 'Ingrese un email valido' })
  email: string;

  @ApiProperty({
    example: 'Karencita123*',
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  password: string;
}
