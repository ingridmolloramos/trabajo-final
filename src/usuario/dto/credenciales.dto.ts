import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MinLength } from "class-validator";

export class credencialesDTO{
    @IsString()
    @IsNotEmpty()
    @ApiProperty({
        example: 'karen',
        required: true,
      })
    nombreUsuario:string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({
        example: 'Karen123*',
        required: true,
      })
    @MinLength(6,{
        message: 'El password del usuario deberia contener al menos 6 caracteres',
    })
    password: string;

}