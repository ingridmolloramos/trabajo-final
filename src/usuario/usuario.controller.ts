import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { UsuarioService } from './usuario.service';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth-guard';
import { ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import { TareaDto } from 'src/tarea/tarea.dto';
import { PaginacionDto } from './dto/paginacion.dto';

@ApiTags('Usuario controller')
@Controller('usuarios')
export class UsuarioController {
  constructor( private readonly usuarioService: UsuarioService) {}
  
  @ApiResponse({ status: 201, description: 'Usuario creado exitosamente' })
  @ApiResponse({ status: 401, description: 'No Autorizado' })
  @ApiResponse({ status: 403, description: 'Usuario no encontrado' })

  @UseGuards(JwtAuthGuard)
  @Post()
  @ApiBody({
    type: CreateUsuarioDto,
    description: 'Estructura JSON para el objeto CreateUsuarioDto',
  })
  async create(@Body() createUsuarioDto: CreateUsuarioDto) {
    return await this.usuarioService.create(createUsuarioDto);
  }
  
  @Get('/:id')
  findOne(@Param('id') id: number) {
    return this.usuarioService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async findAll() {
    return await this.usuarioService.findAll();
  }

  @UseGuards(JwtAuthGuard)
  @Patch('/:id')
  async update(
    @Param('id') id: number,
    @Body() updateUsuarioDto: UpdateUsuarioDto,
  ) {
    return this.usuarioService.update(id, updateUsuarioDto);
  }
  

  @UseGuards(JwtAuthGuard)
  @Delete('/:id')
  async remove(@Param('id') id: number) {
    return await this.usuarioService.remove(id);
  }

  
  @Post('/:id')
  async crearTarea(@Param('id') id: number, @Body() tarea: TareaDto) {
    return await this.usuarioService.crearTarea(id, tarea);
  }

  @Get('/:id/listar-tareas')
  async listarTareas(
    @Param('id') id: number,
    @Body() paginacion: PaginacionDto,
  ) {
    return await this.usuarioService.listarTareas(id, paginacion);
  }

  @Get('/imagen/gatos')
  async gatos() {
    return await this.usuarioService.gatos();
  }
}
