import { Logger, MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { UsuarioService } from './usuario.service';
import { UsuarioController } from './usuario.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Usuario } from './entities/usuario';
import { UsuarioRepository } from './usuario.repository';
import { TareaModule } from 'src/tarea/tarea.module';
import { CatmoduleModule } from 'src/catmodule/catmodule.module';
import { PerfilService } from './perfil.service';
import { PerfilRepository } from './perfil.repository';
import { CatService } from 'src/catmodule/catservice.service';


@Module({
  imports: [TypeOrmModule.forFeature([Usuario]), TareaModule, CatmoduleModule],
  controllers: [UsuarioController],
  providers: [UsuarioService, UsuarioRepository, PerfilService, PerfilRepository ],
})
export class UsuarioModule {}
