import { Injectable } from '@nestjs/common';
import { PerfilDto } from './dto/perfil.dto';
import { PerfilRepository } from './perfil.repository';

@Injectable()
export class PerfilService {
  constructor(private perfilRepository: PerfilRepository) {}
  async crear(perfilDto: PerfilDto) {
    const perfil = this.perfilRepository.crear(perfilDto);
    return perfil;
  }
}
