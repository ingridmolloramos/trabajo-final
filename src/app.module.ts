import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsuarioModule } from './usuario/usuario.module';
import { Usuario } from './usuario/entities/usuario';
import { LoggerMiddleware } from './middleware/logger.middleware';
import { AuthModule } from './auth/auth.module';
import { Perfil } from './usuario/entities/perfil.entity.ts';
import { Roles } from './usuario/entities/roles.entity.ts';
import { Tarea } from './usuario/entities/tarea.entity.ts';
import { Categoria } from './usuario/entities/categoria.entity';
import { TareaModule } from './tarea/tarea.module';
import { CatmoduleModule } from './catmodule/catmodule.module';


import * as dotenv from 'dotenv';
dotenv.config();

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.POSTGRES_IP,
      port: parseInt(process.env.POSTGRES_PORT),
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DATABASE,
      entities: [Usuario, Perfil, Roles, Tarea, Categoria],
      synchronize: true,
      logging: true,
    }), 
    UsuarioModule,
    AuthModule,
    TareaModule, 
    CatmoduleModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule{
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('*') ;
  }
}
