import { Body, Controller, Post } from '@nestjs/common';
import { credencialesDTO } from 'src/usuario/dto/credenciales.dto';
import { UsuarioService } from 'src/usuario/usuario.service';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
    constructor(
        private usuarioService: UsuarioService,
        private authService: AuthService,
    ){}
    
    @Post('/login')
    async login(@Body () credencialesDTO: credencialesDTO){
        const usuario  = await this.usuarioService.validarUsuario(
            credencialesDTO.nombreUsuario,
            credencialesDTO.password,
        );
        return this.authService.login(usuario);
    };
}