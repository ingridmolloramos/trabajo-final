<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Descripción  Tarea


Crear una aplicación de autenticación de usuarios con JWT y proteger las rutas con guards
en NestJS.
Aplicación:
1. Creación del proyecto en NestJS (API Rest):
Iniciar un nuevo proyecto en NestJS para construir una API Rest en Node.js con TypeScript.
2. Implementación de controladores, servicios y repositorios:
Organizar la lógica de negocio con controladores para manejar las solicitudes HTTP,
servicios para la lógica de la aplicación y repositorios para interactuar con la base de datos.
3. Definición de entidades y DTOs (Objetos de Transferencia de Datos):
Crear entidades que representen los modelos de datos, como Usuario, Password, etc., y
definir DTOs para especificar la estructura de los datos transmitidos entre cliente y servidor.
4. Configuración de la base de datos en PostgreSQL y conexión con la
aplicación:
Configurar una base de datos en PostgreSQL para almacenar y gestionar la información de
usuarios y tokens JWT, estableciendo la conexión con la aplicación NestJS para
operaciones CRUD.
5. Definir relaciones en la base de datos:
Definir relaciones entre las entidades de tu base de datos utilizando TypeORM. Esto implica establecer relaciones uno a uno, uno a muchos, muchos a uno y muchos a muchos según lo requiera tu modelo de datos. El modelo debe incluir una entidad
USUARIO y una entidad ROLES (ej. Un usuario puede tener uno o varios roles “admin, usuario, etc”)
6. Implementación de middleware:
Utilizar middleware para interceptar y manipular solicitudes y respuestas HTTP, incluyendo la verificación de ROLES de usuario antes de que las solicitudes lleguen a los controladores.
7. Desarrollo de una estrategia de autenticación con JWT:
Crear una estrategia de autenticación basada en JWT para permitir el inicio de sesión seguro de los usuarios, incluyendo la verificación de ROLES de usuario durante el proceso de autenticación..
8. Protección de las rutas con guards que extienden de la estrategia JWT:
Implementar guards para restringir el acceso a ciertas rutas de la API basados en los roles de usuario proporcionados en los tokens JWT. Estos guards extenderán la estrategia JWT para validar la autenticidad de los tokens JWT proporcionados por los usuarios y verificar los roles asociados con el usuario autenticado.
9. Desarrollar la lógica para consumir datos de una API externa:
Implementación de un módulo para consumir datos de una API externa. Puedes utilizar cualquier API pública disponible para este propósito (https://github.com/public-apis/public-apis) 10.Integrar Swagger para documentación de la API:
Integrar Swagger en tu proyecto NestJS para generar documentación automática e interactiva de tu API. Esto implicará documentar todos tus endpoints, incluidos los nuevos que has creado para consumir datos de la API externa.
11. Implementar pruebas unitarias y de integración:
Desarrollar pruebas unitarias para verificar el funcionamiento correcto de tus controladores, servicios. Utiliza mocks para simular el comportamiento de dependencias externas, como la base de datos y la API externa.
12. Implementar variables de entorno (OPCIONAL)
Opcionalmente, se puede optar por implementar variables de entorno en el proyecto. Esto permitirá la configuración dinámica de diferentes aspectos de la aplicación, como la conexión a la base de datos, la configuración de Swagger y la integración con APIs externas.


## Instalación

```bash
$ npm install
```

## Para correr la aplicación

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```


